from moviepy.editor import VideoFileClip
from openai import OpenAI
from langchain_openai import ChatOpenAI
from langchain.text_splitter import CharacterTextSplitter # used for splitter the text into smalle chunks
from langchain.docstore.document import Document # convert the chunks in document format
from langchain.chains.summarize import load_summarize_chain # connect prompt and llm model
from fastapi import FastAPI, UploadFile, HTTPException, File
from fastapi.middleware.cors import CORSMiddleware
from tempfile import NamedTemporaryFile
import shutil
from pydantic import BaseModel
import tempfile
 
#Enter your OPENAI api key here
openai_api_key = ""
app = FastAPI()
 
app.add_middleware(
 CORSMiddleware,
 allow_origins=["http://localhost:3000"], # Add your frontend origin here
 allow_credentials=True,
 allow_methods=["*"],
 allow_headers=["*"],
 )
 

def process_video(video_file):
    # Extracting audio from video
    video = VideoFileClip(video_file)
    audio_temp_file = tempfile.NamedTemporaryFile(suffix=".mp3", delete=False)
    audio_temp_file_path = audio_temp_file.name
    video.audio.write_audiofile(audio_temp_file_path)
    audio_temp_file.close()
    # OpenAI to transcribe audio
    client = OpenAI(api_key=openai_api_key)
    try:
      with open(audio_temp_file_path, "rb") as audio_file:
        transcription = client.audio.transcriptions.create(
        model="whisper-1",
        file=audio_file,
        response_format="text"
        )
      text_splitter = CharacterTextSplitter(chunk_size=50, chunk_overlap=20)
      texts = text_splitter.split_text(transcription) # split the text into smaller chunks
      docs = [Document(page_content=t) for t in texts]
      llm = ChatOpenAI(temperature=0, model_name="gpt-3.5-turbo-1106", api_key = openai_api_key)
      chain = load_summarize_chain(llm, chain_type="stuff")
      summary = chain.run(docs)
      return summary
    except Exception as e:
        return str(e)

 
@app.post("/process-video")
async def upload_and_process(video_file: UploadFile = File(...)):
    # Save uploaded video
    # await video_file.write("video.mp4")
    try:
       with NamedTemporaryFile(delete=False) as temp_video:
           shutil.copyfileobj(video_file.file, temp_video)
       summary = process_video(temp_video.name)
       return {"summary" : summary}

    except Exception as e:
       raise HTTPException(status_code=500, detail=str(e))


@app.get("/home")
def home():
    return "hi"

    
 
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000, reload=True)