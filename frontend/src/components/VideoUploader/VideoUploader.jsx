import React, { useRef, useState } from "react";
import "./VideoUploader.css";
import uploadImg from "../../assets/cloud-upload-regular-240.png";
import mp4File from "../../assets/mp4.png";
import delFile from "../../assets/close.png";
import PropTypes from "prop-types";
import axios from "axios";

const VideoUploader = (props) => {
  const [file, setFile] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");
  const wrapperRef = useRef(null);

  const onDragEnter = () => wrapperRef.current.classList.add("dragover");
  const onDragLeave = () => wrapperRef.current.classList.remove("dragover");
  const onDrop = () => wrapperRef.current.classList.remove("dragover");

  const onFileDrop = (e) => {
    const newFile = e.target.files[0];
    if (file) {
      setErrorMessage("Can't upload multiple videos.");
    } else if (newFile && newFile.type == "video/mp4") {
      console.log(newFile);
      setFile(newFile);
      props.onFileChange(newFile);
      //send file to backend for processing
      handleVideoUpload(newFile);
    } else {
      console.error("Invalid file type");
    }
  };

  const handleVideoUpload = async (selectedFile) => {
    const formData = new FormData();
    formData.append("video_file", selectedFile); //add video file to formdata
    console.log(selectedFile);
    try {
      const response = await axios.post(
        "http://localhost:8000/process-video",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log("reponse ", response);
      if (response && response.data && response.data.summary) {
        props.onSummaryChange(response.data.summary); //pass summary to meeting summary
      } else {
        throw new Error("Invalid response format");
      }

      console.log("response", response);
      setErrorMessage(""); //clear error message
    } catch (error) {
      console.error("Error uploading video: ", error);
      setErrorMessage("An error occurred while processing the video.");
    }
  };

  const fileRemove = (file) => {
    console.log(file);
    setFile(null);
    setErrorMessage("");
    props.onFileChange(null);
    props.onSummaryChange(null); //clear summary when removing file
  };

  const formatFileSize = (size) => {
    if (size >= 1024 * 1024) {
      return (size / (1024 * 1024)).toFixed(2) + " MB";
    } else if (size >= 1024) {
      return (size / 1024).toFixed(2) + " KB";
    } else {
      return size + " bytes";
    }
  };

  return (
    <div className="box">
      <h2 className="header">Upload Meeting Video</h2>
      <div
        ref={wrapperRef}
        onDragEnter={onDragEnter}
        onDragLeave={onDragLeave}
        onDrop={onDrop}
        className="drop-file-input"
      >
        <div className="drop-file-input_label">
          <img src={uploadImg} alt="cloud-upload-regular" />
          <p>Drag and Drop your file here</p>
        </div>
        <input type="file" value="" onChange={onFileDrop} accept=".mp4" />
      </div>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      {file && (
        <div className="drop-file-preview">
          <img src={mp4File} alt="" />
          <div className="drop-file-preview_item_info">
            <p>
              <b>File Name - </b>
              {file.name}
            </p>
            <p>
              <b>File Size - </b>
              {formatFileSize(file.size)}
            </p>
          </div>
          <span
            className="drop-file-preview_item_del"
            onClick={() => fileRemove(file)}
          >
            <img src={delFile} alt="" />
          </span>
        </div>
      )}
    </div>
  );
};

VideoUploader.propTypes = {
  onFileChange: PropTypes.func,
};

export default VideoUploader;
