import React from "react";
import './MeetingSummary.css'
const MeetingSummary = (props) => {
  const { summary } = props;

  if (!summary) {
    return <p>No Summary available yet.</p>;
  }

  //Handle Different summary formats (text, list)
  if (Array.isArray(summary)) {
    return (
      <ul className="summary-p">
        {summary.map((point, index) => (
          <li key={index}>{point}</li>
        ))}
      </ul>
    );
  } else {
    return (
      <>
      <h3 style={{margin: "10px"}}>Summary</h3>
      <p className="summary-p">{summary}</p>
      </>
    
    ) 
    
  }
};

export default MeetingSummary;
