import React, { useState } from "react";
import "./App.css";
import VideoUploader from "./components/VideoUploader/VideoUploader";
import MeetingSummary from "./components/MeetingSummary/MeetingSummary";
import Loader from "react-js-loader";
 
function App() {
  const [summary, setSummary] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
 
  const onSummaryChange = (newSummary) => {
    setSummary(newSummary);
    setIsLoading(false);
  };
 
  const onFileChange = (files) => {
    console.log(files);
    setIsLoading(true);
  };
 
  return (
    <>
      <VideoUploader
        onFileChange={(files) => onFileChange(files)}
        onSummaryChange={onSummaryChange}
      />
      {isLoading ? (
        <Loader
          type="spinner-default"
          bgColor="rgb(100, 149, 237)"
          color="black"
          title="Loading Summary..."
          size={85}
        />
      ) : summary ? (
        <MeetingSummary summary={summary} />
      ) : null}
    </>
  );
}
 
export default App;
 